#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#include "Directx.h"
#include "Timer.h"
#include "DirectInput.h"
#include <string> 

Directx directx;
const char g_szClassName[] = "MainWindow";

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return 0;
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	WNDCLASSEX wc;
	HWND hwnd;
	MSG Msg;


	//Step 1: Registering the Window Class
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = g_szClassName;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	
	if (!RegisterClassEx(&wc))
	{
		MessageBox(NULL, "Window Registration Failed!", "Error!",
			MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	// Step 2: Creating the Window
	hwnd = CreateWindowEx(
		WS_EX_CLIENTEDGE,
		g_szClassName,
		"The title of my window",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, 1000, 1000,
		NULL, NULL, hInstance, NULL);

	if (hwnd == NULL)
	{
		MessageBox(NULL, "Window Creation Failed!", "Error!",
			MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);


	Timer timer;
	DirectInput input;
	if (!input.InitDirectInput(hInstance))
	{
		MessageBox(0, "Direct Input Initialization - Failed",
			"Error", MB_OK);
		return 0;
	}

	input.hwnd = &hwnd;
	input.InitDevices();

	//input.DIKeyboard->SetDataFormat()


	directx.mhMainWindow = hwnd;
	directx.createDevice();
	directx.shaderStage();
	directx.initGraphics();
	directx.createGround();
	directx.CreateSphere(10, 10);



	// Step 3: The Message Loop


	while (true)
	{
		BOOL PeekMessageL(
			LPMSG lpMsg,
			HWND hWnd,
			UINT wMsgFilterMin,
			UINT wMsgFilterMax,
			UINT wRemoveMsg
			);

		if (PeekMessage(&Msg, NULL, 0, 0, PM_REMOVE))
		{
			if (Msg.message == WM_QUIT)
				break;
			TranslateMessage(&Msg);
			DispatchMessage(&Msg);
		}
		else{
			directx.frameCount++;
			if (timer.GetTime() > 1.0f)
			{
				directx.fps = directx.frameCount;
				directx.frameCount = 0;
				timer.StartTimer();

				SetWindowText(hwnd, ("FPS: " + std::to_string(directx.fps)).c_str());
			}

			timer.frameTime = timer.GetFrameTime();
			//input.DetectInput(timer.frameTime);
			directx.UpdateScene(timer.frameTime);
			directx.DrawScene();
		}

	}
	return Msg.wParam;
}

