TextureCube  SkyMap: register(t0);
SamplerState smpler : register(s0);

struct IN    //output structure for skymap vertex shader
{
	float4 Pos : SV_POSITION;
	float3 texCoord : TEXCOORD;
};

float4 main(IN input) : SV_TARGET
{
	//return float4(1.0f, 0.0f, 1.0f, 1.0f);
	return SkyMap.Sample(smpler, input.texCoord);
}