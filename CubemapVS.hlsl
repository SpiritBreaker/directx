cbuffer cbPerObject
{
	float4x4 WVP;
	float4x4 World;
};

struct IN
{
	float4 position : POSITION;
	float2 UV : TEXCOORD;
};

struct OUT
{
	float4 position : SV_POSITION;
	float3 UV : TEXCOORD;
};

OUT main(IN input)
{

	OUT output;
	// Change the position vector to be 4 units for proper matrix calculations.
	output.position = mul(input.position, WVP).xyww;
	output.UV = input.position;
	return output;
}
