#pragma once
#include <dinput.h>
#include <xnamath.h>

class DirectInput
{
public:
	IDirectInputDevice8* DIKeyboard;
	IDirectInputDevice8* DIMouse;
	HWND *hwnd;

	DIMOUSESTATE mouseLastState;
	LPDIRECTINPUT8 DInput;

	XMMATRIX Rotationx;
	XMMATRIX Rotationz;

	bool InitDirectInput(HINSTANCE hInstance);
	void InitDevices();
	void DetectInput(double time);

	DirectInput();
	~DirectInput();
};

