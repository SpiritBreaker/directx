Texture2D tx : register(t0);
SamplerState smpler : register(s0);

struct Light
{
	float3 dir;
	float3 pos;
	float  range;
	float3 att;
	float4 ambient;
	float4 diffuse;
};
cbuffer cbPerFrame
{
	Light light;
};

struct IN
{
	float4 position : SV_POSITION;
	float4 worldPos : POSITION;
	float2 UV : TEXCOORD;
	float3 normal : NORMAL;
	

};

float4 main(IN input) : SV_TARGET
{

	input.normal = normalize(input.normal);

	float4 diffuse = tx.Sample(smpler, input.UV);

	float3 finalColor = float3(0.0f, 0.0f, 0.0f);

	float3 lightToPixelVec = light.pos - input.worldPos;

	float d = length(lightToPixelVec);

	float3 finalAmbient = diffuse * light.ambient;

	if (d > light.range)
		return float4(finalAmbient, diffuse.a);

	lightToPixelVec /= d;

	float howMuchLight = dot(lightToPixelVec, input.normal);

	if (howMuchLight > 0.0f)
	{
		//Add light to the finalColor of the pixel
		finalColor += howMuchLight * diffuse * light.diffuse;

		//Calculate Light's Falloff factor
		finalColor /= light.att[0] + (light.att[1] * d) + (light.att[2] * (d*d));
	}

	finalColor = saturate(finalColor + finalAmbient);

	return float4(finalColor, diffuse.a);
}
