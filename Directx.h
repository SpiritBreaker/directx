#pragma once
#include <D3D11.h>
#include <d3dx11.h>

#include <vector>

class Directx
{
public:

	HRESULT hr;

	bool createDevice();
	bool mEnable4xMSAA = true;
	HWND mhMainWindow;

	IDXGISwapChain* mSwapChain;
	ID3D11Device* m3dDevice;
	ID3D11DeviceContext* md3dImmediateContext;
	ID3D11RenderTargetView* mRenderTargetView;
	UINT m4xMsaaQuality;
	DXGI_SWAP_CHAIN_DESC sd;

	ID3D11Buffer* triangleVertBuffer;
	ID3D11Buffer* squareIndexBuffer;
	ID3D11VertexShader* VS;
	ID3D11PixelShader* PS;
	char* VS_bytes, *PS_bytes;
	size_t VS_size, PS_size;
	ID3D11InputLayout *vertLayout;

	ID3D11Buffer* cbPerObjectBuffer;
	ID3D11Buffer* cbPerFrameBuffer;

	ID3D11Texture2D* mDepthStencilBuffer;
	ID3D11DepthStencilView* mDepthStencilView;

	ID3D11Texture2D* backBuffer;
	
	ID3D11RasterizerState* WireFrame;

	//Cube texture
	ID3D11ShaderResourceView* CubesTexture;
	ID3D11SamplerState* samplerState;

	UINT stride;
	UINT offset;

	void DrawScene();
	void shaderStage();
	void initGraphics();
	void initPipeline();

	float red = 0.0f;
	float green = 0.0f;
	float blue = 0.0f;

	int colormodr = 1;
	int colormodg = 1;
	int colormodb = 1;
	float rot;

	void ReadAllBytes(char const* filename, char*& bytes, size_t &size);
	void UpdateScene(double time);

	//Create ground
	ID3D11Buffer* groundIndexBuffer;
	ID3D11Buffer* groundVertBuffer;
	ID3D11ShaderResourceView* groundTexture;
	void createGround();

	//Create Sphere
	ID3D11Buffer* sphereIndexBuffer;
	ID3D11Buffer* sphereVertBuffer;
	ID3D11VertexShader* Cubemap_VS;
	ID3D11PixelShader* Cubemap_PS;
	char* Cubemap_VS_bytes, *Cubemap_PS_bytes;
	size_t Cubemap_VS_size, Cubemap_PS_size;
	ID3D11SamplerState* CubesTexSamplerState;
	ID3D11ShaderResourceView* smrv;
	ID3D11DepthStencilState* DSLessEqual;
	ID3D11RasterizerState* RSCullNone;
	int NumSphereVertices;
	int NumSphereFaces;

	void CreateSphere(int LatLines, int LongLines);

	int frameCount = 0;
	int fps = 0;

	Directx();
	~Directx();
};

