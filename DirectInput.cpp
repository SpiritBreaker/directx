#include "DirectInput.h"
#include <stdio.h>

bool DirectInput::InitDirectInput(HINSTANCE hInstance)
{
	HRESULT hr;
	hr = DirectInput8Create(
		hInstance,
		DIRECTINPUT_VERSION,
		IID_IDirectInput8,
		(void**)&DInput,
		NULL);

	return true;
}

void DirectInput::DetectInput(double time)
{
	DIMOUSESTATE mouseCurrState;
	BYTE keyboardState[256];
	DIKeyboard->Acquire();
	DIMouse->Acquire();

	DIMouse->GetDeviceState(sizeof(DIMOUSESTATE), &mouseCurrState);
	DIKeyboard->GetDeviceState(sizeof(keyboardState), (LPVOID)&keyboardState);
	
	if (keyboardState[DIK_ESCAPE] & 0x80)
		PostMessage(*hwnd, WM_DESTROY, 0, 0);

	if (keyboardState[DIK_A] & 0x80)
	{
		printf("DIK_LEFT\n");
	}
	if (keyboardState[DIK_D] & 0x80)
	{
		printf("DIK_RIGHT\n");
	}
	if (keyboardState[DIK_W] & 0x80)
	{
		printf("DIK_UP\n");
	}
	if (keyboardState[DIK_S] & 0x80)
	{
		printf("DIK_DOWN\n");
	}
	if (mouseCurrState.lX != mouseLastState.lX)
	{
		printf("Mouse\n");
	}
	if (mouseCurrState.lY != mouseLastState.lY)
	{
		printf("Mouse\n");
	}

	mouseLastState = mouseCurrState;

	return;
}


void DirectInput::InitDevices()
{
	DInput->CreateDevice(GUID_SysKeyboard,
		&DIKeyboard,
		NULL);

	DInput->CreateDevice(GUID_SysMouse,
		&DIMouse,
		NULL);

	DIKeyboard->SetDataFormat(&c_dfDIKeyboard);
	DIKeyboard->SetCooperativeLevel(*hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);

	DIMouse->SetDataFormat(&c_dfDIMouse);
	DIMouse->SetCooperativeLevel(*hwnd, DISCL_NONEXCLUSIVE | DISCL_NOWINKEY | DISCL_FOREGROUND);

}


DirectInput::DirectInput()
{
}


DirectInput::~DirectInput()
{
}
