cbuffer cbPerObject
{
	float4x4 WVP;
	float4x4 World;
};


struct IN
{
	float4 position : POSITION;
	float2 UV : TEXCOORD;
	float3 normal: NORMAL;
};

struct OUT
{
	float4 position : SV_POSITION;
	float4 worldPos: POSITION;
	float2 UV : TEXCOORD;
	float3 normal: NORMAL;
};

OUT main(IN input)
{

	OUT output;
	// Change the position vector to be 4 units for proper matrix calculations.
	output.position = mul(input.position, WVP);
	output.worldPos = mul(input.position, World);
	output.normal = mul(input.normal, World);
	output.UV = input.UV;


	return output;
}
