#pragma once


class Timer
{


public:
	Timer();
	~Timer();

	__int64 frameTimeOld = 0;
	double frameTime;

	double countsPerSecond = 0.0;
	__int64 CounterStart = 0;

	void StartTimer();
	double GetTime();
	double GetFrameTime();

};

